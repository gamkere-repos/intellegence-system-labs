import janus_swi as janus

class Prolog_provider:
    def __init__(self, prologDbPath: str) -> None:
        self.prologDb = prologDbPath
        janus.consult(self.prologDb)


    def create(self, attributes: dict) -> dict:
        query = janus.query_once('assert(car(Model, Country, Year, Transmission, Body, Fuel))', attributes)
        janus.query_once('save_file')
        return query

    def get_countries(self):
        return janus.query_once('get_country(Country)', {})

    def get_transmission_type(self) -> dict:
        return janus.query_once('get_transmission(Transmission)', {})

    def consult(self, attributes: dict) -> dict:
        query = janus.query_once('get_models(Country, Transmission, Fuel, Body, Models)', attributes)
        return query

    def get_body_type(self):
        return janus.query_once('get_body(Bodies)')

    def get_fuel(self):
        return janus.query_once('get_fuel(Fuels)')

    def get_all_models(self):
        return janus.query_once('get_all_models(Models)')