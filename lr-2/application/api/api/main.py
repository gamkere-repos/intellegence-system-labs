from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel, ConfigDict


from prolog_provider.prolog_provider import Prolog_provider

BASE_PATH_DK = '../base/rules.pl'

class Car(BaseModel):
    Model: str
    Country: str
    Year: str
    Body: str
    Fuel: str
    Transmission: str

class BaseSchema(BaseModel):
    model_config = ConfigDict(from_attributes=True)

class PostSchema(BaseSchema):
    truth: bool



class Country(BaseModel):
    Country: str

class CountrySchema(BaseSchema):
    Countries: list[Country]


class Transmission(BaseModel):
    Transmission_type: str

class TransmissionSchema(BaseSchema):
    Transmission_types: list[Transmission]

class Fuel(BaseModel):
    Fuel: str

class FuelSchema(BaseSchema):
    Fuels: list[Fuel]

class Body(BaseModel):
    Body: str

class BodySchema(BaseSchema):
    Bodies: list[Body]

app = FastAPI()


origins = [
    "http://localhost",
    "http://localhost:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.post("/car/create")
async def create_car(car: Car) -> PostSchema:
    provide = Prolog_provider(BASE_PATH_DK)
    result = provide.create(dict(car))
    return PostSchema(truth=result['truth'])

@app.get("/car/country")
async def get_country() -> CountrySchema:
    provide = Prolog_provider(BASE_PATH_DK)
    countries = provide.get_countries()
    return CountrySchema(
            Countries=[Country(Country=country) for country in countries['Country']])

@app.get("/car/transmission")
async def get_transmission() -> TransmissionSchema:
    provide = Prolog_provider(BASE_PATH_DK)
    transmissions = provide.get_transmission_type()
    return TransmissionSchema(Transmission_types=[Transmission(Transmission_type=transmission) for transmission in transmissions['Transmission']])

@app.get("/car/fuel")
async def get_fuel() -> FuelSchema:
    provide = Prolog_provider(BASE_PATH_DK)
    fuels = provide.get_fuel()
    return FuelSchema(Fuels=[Fuel(Fuel=fuel) for fuel in fuels['Fuels']])

@app.get("/car/body")
async def get_fuel() -> BodySchema:
    provide = Prolog_provider(BASE_PATH_DK)
    bodies = provide.get_body_type()
    return BodySchema(Bodies=[Body(Body=body) for body in bodies['Bodies']])

@app.get("/car")
async def get_cars(country: str, transmission: str, fuel: str, body: str)-> list[Car]:
    provide = Prolog_provider(BASE_PATH_DK)

    params = {
            "Country": country,
            "Transmission": transmission,
            "Body": body,
            "Fuel": fuel,
    }

    cars = provide.consult(params)

    return [
        Car(
            Model=car[0],
            Country=car[1],
            Year=str(car[2]),
            Transmission=car[3],
            Body=car[4],
            Fuel=car[5]
        ) for car in cars['Models']]