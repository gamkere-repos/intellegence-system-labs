:- ['database.pl'].

save_file:- tell('../base/database.pl'), listing(car(_,_,_,_,_,_)),told.

print_enumeration_list([], _Number):-!.
print_enumeration_list([Head|Tail], Number):-
    write('\t'), write(Number), write(' - '), write(Head), nl,
    Next is Number+1,
    print_enumeration_list(Tail, Next).

unique([]):-!.
unique([Head|Tail]):-
   member(Head, Tail), !, fail;
   unique(Tail).

list_to_set(List, Set):-
    list_to_set(List, [], ReverseSet),
    reverse(ReverseSet, Set).

list_to_set([], Buffer, Buffer):-!.
list_to_set([Head|Tail], Buffer, Set):-
  member(Head, Buffer), !, list_to_set(Tail, Buffer, Set);
  list_to_set(Tail, [Head|Buffer], Set).

get_country(Set):-
    findall(Country, car(_, Country, _, _, _, _), Countries),
    list_to_set(Countries, Set).

get_transmission(Set):-
    findall(Transmission, car(_, _, _, Transmission, _, _), Transmissions),
    list_to_set(Transmissions, Set).

get_body(Set):-
    findall(Body, car(_, _, _, _, Body, _), Bodies),
    list_to_set(Bodies, Set).

get_fuel(Set):-
    findall(Fuel, car(_, _, _, _, _, Fuel), Fuel_type),
    list_to_set(Fuel_type, Set).

get_all_models(Set):-
    findall(Model, car(Model, _, _, _, _, _), Models),
    list_to_set(Models, Set).

get_models(Country, Transmission, Fuel, Body, Models):-
    findall([Model, Country, Year, Transmission, Body, Fuel], car(Model, Country, Year, Transmission, Body, Fuel), Set),
    list_to_set(Set, Models).