// берём Express
var express = require('express');

// создаём Express-приложение
var app = express();


// создаём маршрут для главной страницы
// http://localhost:8080/
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist/'));
app.use('/css', express.static(__dirname + '/css/'));
app.get('/', function(req, res) {
  res.sendfile('index.html');
});
app.get('/css/', function(req, res) {
    res.sendFile('style.css');
});

// запускаем сервер на порту 8080
app.listen(8080);
// отправляем сообщение
console.log('Сервер стартовал!');