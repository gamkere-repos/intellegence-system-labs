const Patient = require("../models/Patient")

exports.addPatient = function (request, response) {
    response.render('patients/form', {title: "Добавить пациента", active: "patients",isNewRecord: true})
}

exports.createPatient = function (request, response) {
    if (!request.body) return response.sendStatus(400);
    Patient.create(request.body).then(() => {
        response.redirect("/patients");
    }).catch(err=>console.log(err));
}

exports.getInfo = function (request, response) {
    const id = request.params.id;
    const map = {
        resting_ecg: {0: "LVH", 1: "ST", 2: "В норме"},
        pain_type: {0: "ASY", 1: "NAP", 2: "ATA", 3: "TA"},
        yes_no: {0: "Нет", 1: "Да"},
        sex: {1: "Мужской", 0: "Женский"}
    };
    Patient.findOne({where: {id: id}}).then((data) => {
        response.render('patients/info', {title: "Подробная информация", active: "patients", isNewRecord: false, patient: data, map: map})
    }).catch(err=>console.log(err));
}

exports.refreshPatient = function (request, response) {
    const id = request.params.id;
    Patient.findOne({where: {id: id}}).then((data) => {

        response.render('patients/form', {title: "Обновление данных пациента", active: "patients", isNewRecord: false, patient: data})
    }).catch(err=>console.log(err));
}

exports.updatePatient = function (request, response) {
    if (!request.body) return response.sendStatus(400);
    id = request.body.id;
    Patient.update(request.body, {where: {id: id}}).then(() => {
        response.redirect("/patients");
    }).catch(err=>console.log(err));
}

exports.deletePatient = function (request, response) {
    const userid = request.params.id;
    Patient.destroy({where: {id: userid}}).then(() => {
        response.redirect("/patients")
    }).catch(err=>console.log(err));
}

exports.index = function (request, response) {
    Patient.findAll({raw: true}).then(data => {
        response.render('patients/index', {title: "Пациенты", active: 'patients', patients: data})
    }).catch(error=>console.log(error));
}