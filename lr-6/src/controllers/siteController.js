exports.main = function (request, response) {
    response.render('main', {title: "Главная", active: "main"})
}

exports.predict = function (request, response) {
    response.render('predict.ejs', {title: "Прогнозирование", active: "predict"})
}