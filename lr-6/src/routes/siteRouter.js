const express = require('express');
const siteController = require('../controllers/siteController.js');
const siteRouter = express.Router();

siteRouter.get('/', siteController.main);
siteRouter.get('/predict', siteController.predict)

module.exports = siteRouter;