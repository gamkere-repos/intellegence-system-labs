const express = require('express');
const patientController = require('../controllers/patientController.js');
const patientRouter = express.Router();
const urlencodedParser = express.urlencoded({extended: false});

patientRouter.get('/create', patientController.addPatient);
patientRouter.post('/create', urlencodedParser, patientController.createPatient);
patientRouter.get('/', patientController.index);
patientRouter.get('/info/:id', patientController.getInfo);
patientRouter.get('/delete/:id', urlencodedParser, patientController.deletePatient);
patientRouter.get('/update/:id', patientController.refreshPatient);
patientRouter.post('/update', urlencodedParser, patientController.updatePatient);

module.exports = patientRouter;