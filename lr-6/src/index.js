const express = require('express');
const patientRouter = require("./routes/patientRouter.js")
const siteRouter = require("./routes/siteRouter.js");

const db = require('./db.js');

const app = express();
const port = 3000;

app.set('view engine', 'ejs');
app.use(express.urlencoded({ extended: false }));

app.use('/', siteRouter);
app.use('/patients', patientRouter);

db.sync({ force: false }).then(() => {
    app.listen(port, console.log("Server started http://localhost:3000"));
});