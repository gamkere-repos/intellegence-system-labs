const { Sequelize, DataTypes } = require("sequelize");
const sequelize = require("../db");


module.exports = sequelize.define('patient', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    fio: {
        type: DataTypes.STRING,
        allowNull: false
    },
    age: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    sex: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    pain_type: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    resting_bp: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    cholesterol: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    fasting_bs: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    resting_ecg: {
        type: DataTypes.STRING,
        allowNull: false
    },
    max_hr: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    exercise_angina: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    heart_disease: {
        type: DataTypes.INTEGER,
        allowNull: true
    }
});
