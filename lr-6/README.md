### Запуск сайта

Установить node.js https://nodejs.org/en/download/package-manager версии 16.20.2

Перейти через терминал в папку с проектом: lr-6/src

Выполнить команду

```
npm install
```

После того как установятся зависимости выполнить команду
```
node index.js
```

сайт будет доступен по адресу
http://localhost:3000


Структура:
```
.
├── controllers // контроллеры
│   ├── patientController.js
│   └── siteController.js
├── db.js // коннтект с бд
├── heart.sqlite // бд
├── index.js // точка входа
├── models // модель
│   └── Patient.js
├── package.json // зависимости
├── package-lock.json
├── routes // маршруты
│   ├── patientRouter.js
│   └── siteRouter.js
└── views // html код
    ├── main.ejs
    ├── partials
    │   ├── footer.ejs
    │   ├── head.ejs
    │   └── menu.ejs
    └── patients
        ├── form.ejs
        ├── index.ejs
        ├── info.ejs
        └── predict.ejs
```