# Лабораторная работа № 7

Структура проекта:
```
├── datasets
│   ├── bank_deposit_train.csv
│   └── heart.csv
├── decision_tree_bank.ipynb //Недоработка (не используем)
├── decision_tree_heart.ipynb
├── poetry.lock
└── pyproject.toml
```

Ссылка на Google Collab: [Блокнот](https://drive.google.com/file/d/1fheW-PbPJGQnfKXCJkCBn2ekYi0uQKjz/view?usp=sharing)

## Технологии

В настоящей работе используются следующие технологии с соотвествующими лицензиями.

* Python 3.12, лицензия [Python Software Foundation License](https://www.python.org/psf-landing/)

* Scikit-learn, лицензия [BSD 3-Clause License](https://github.com/scikit-learn/scikit-learn?tab=BSD-3-Clause-1-ov-file)

* Visual Studio Code, лицензия [MIT](https://github.com/microsoft/vscode/blob/dc96b837cf6bb4af9cd736aa3af08cf8279f7685/LICENSE.txt)

* Pandas, лицензия [BSD 3-Clause License](https://github.com/pandas-dev/pandas/blob/main/LICENSE)

## Набор данных

### Источник

Набор данных взят с сайта [Kaggle](https://www.kaggle.com/).

Набор данных используемый в работе [Heart Failure Prediction Dataset](https://www.kaggle.com/datasets/fedesoriano/heart-failure-prediction/data).

Источник:
fedesoriano. (September 2021). Heart Failure Prediction Dataset. Retrieved [Date Retrieved] from https://www.kaggle.com/fedesoriano/heart-failure-prediction.

### Краткое описание

Сердечно-сосудистые заболевания (ССЗ) являются причиной смерти № 1 во всем мире, ежегодно унося, по оценкам, 17,9 миллиона жизней, что составляет 31% всех смертей во всем мире. Четыре из пяти смертей от сердечно-сосудистых заболеваний происходят в результате сердечных приступов и инсультов, и треть этих смертей происходит преждевременно у людей в возрасте до 70 лет. Сердечная недостаточность является распространенным заболеванием, вызванным ССЗ, и этот набор данных содержит 11 признаков, которые могут быть использованы для прогнозирования возможного заболевания сердца.

Люди с сердечно-сосудистыми заболеваниями или с высоким сердечно-сосудистым риском (из-за наличия одного или нескольких факторов риска, таких как гипертония, диабет, гиперлипидемия или уже выявленное заболевание) нуждаются в раннем выявлении и лечении.

### Признаки

* Age: возраст пациента (год, числовое)
* Sex: пол пациента (M: Male (мужской), F: Female (женский))
* ChestPainType: тип боли в груди [TA: Typical Angina, ATA: Atypical Angina, NAP: Non-Anginal Pain, * ASY: Asymptomatic]
* RestingBP: давление кров в состоянии покоя [mm Hg]
* Cholesterol:уровень холестерина [mm/dl]
* FastingBS: уровень сахара [1: if FastingBS > 120 mg/dl, 0: otherwise]
* RestingECG: результат ЭКГ в покое [Normal: Normal, ST: имеются ST-T пики], LVH: наличие вероятной или определенной гипертрофии левого желудочка по критериям Эстеса]
* MaxHR: максимальная ЧСС [Numeric value between 60 and 202]
* ExerciseAngina: стенокардия вызванная физ. нагрузкой [Y: Yes, N: No]
* Oldpeak: oldpeak = ST [Numeric value measured in depression]
* ST_Slope: Дополнительные показатели экг [Up: upsloping, Flat: flat, Down: downsloping]
* HeartDisease: предсказываемое значение [1: heart disease, 0: Normal]